<?php
/**
 * Plugin Name: LearnDash Advance Sorting
 * Plugin URI: http://www.learndash.com
 * Description: LearnDash LMS Plugin Addon- LearnDash Advance Sorting
 * Version: 1.0.0
 * Author: LearnDash
 * Author URI: http://www.learndash.com
 * Text Domain: learndash
 * Doman Path: /languages/
 *
 * @since 2.1.0
 *
 * @package LearnDash
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


class WpProQuiz_Model_Model_New {
	
	/**
	 * @var WpProQuiz_Model_QuizMapper
	 */
	protected $_mapper = null;
	
	public function __construct($array = null) {
		$this->setModelData($array);
	}
	
	public function setModelData($array) {
		if($array != null) {

			//3,4x faster
			$n = explode(' ', implode('', array_map('ucfirst', explode('_', implode(' _', array_keys($array))))));
			
			$a = array_combine($n, $array);
			
			if (isset($a['Id'])) {
				$this->setId($a['Id']);
			}
			
			foreach($a as $k => $v) {
				$this->{'set'.$k}($v);
			}
		}
	}
	
	public function __call($name, $args) {
	}
	
	/**
	 * 
	 * @return WpProQuiz_Model_QuizMapper
	 */
	public function getMapper() {
		if($this->_mapper === null) {
			$this->_mapper = new WpProQuiz_Model_QuizMapper();
		}

		return $this->_mapper;
	}
	
	/** 
	 * @param WpProQuiz_Model_QuizMapper $mapper
	 * @return WpProQuiz_Model_Model
	 */
	public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
}
class WpProQuiz_Model_StatisticHistory_New extends WpProQuiz_Model_Model_New {
	
	protected $_userId = 0;
	protected $_userName = '';
	protected $_statisticRefId = 0;
	protected $_quizId = 0;
	protected $_createTime = 0;
	protected $_correctCount = 0;
	protected $_incorrectCount = 0;
	protected $_points = 0;
	protected $_result = 0;
	protected $_formatTime = '';
	protected $_formatCorrect = '';
	protected $_formatIncorrect = '';
    protected $_gPoints = 0;
    protected $_questionTime = 0;
	
	public function setUserId($_userId) {
		$this->_userId = (int)$_userId;
		return $this;
	}
	
	public function getUserId() {
		return $this->_userId;
	}
	
	public function setUserName($_userName) {
		$this->_userName = (string)$_userName;
		return $this;
	}
	
	public function getUserName() {
		return $this->_userName;
	}
	
	public function setStatisticRefId($_statisticRefId) {
		$this->_statisticRefId = (int)$_statisticRefId;
		return $this;
	}
	
	public function getStatisticRefId() {
		return $this->_statisticRefId;
	}
	
	public function setQuizId($_quizId) {
		$this->_quizId = (int)$_quizId;
		return $this;
	}
	
	public function getQuizId() {
		return $this->_quizId;
	}
	
	public function setCreateTime($_createTime) {
		$this->_createTime = (int)$_createTime;
		return $this;
	}
	
	public function getCreateTime() {
		return $this->_createTime;
	}
	
	public function setCorrectCount($_correctCount) {
		$this->_correctCount = (int)$_correctCount;
		return $this;
	}
	
	public function getCorrectCount() {
		return $this->_correctCount;
	}
	
	public function setIncorrectCount($_incorrectCount) {
		$this->_incorrectCount = (int)$_incorrectCount;
		return $this;
	}
	
	public function getIncorrectCount() {
		return $this->_incorrectCount;
	}
	
	public function setPoints($_points) {
		$this->_points = (int)$_points;
		return $this;
	}
	
	public function getPoints() {
		return $this->_points;
	}
	
	public function setResult($_result) {
		$this->_result = (float)$_result;
		return $this;
	}
	
	public function getResult() {
		return $this->_result;
    }
	
	public function setFormatTime($_formatTime) {
		$this->_formatTime = (string)$_formatTime;
		return $this;
	}
	
	public function getFormatTime() {
		return $this->_formatTime;
	}
	
	public function setFormatCorrect($_formatCorrect) {
		$this->_formatCorrect = (string)$_formatCorrect;
		return $this;
	}
	
	public function getFormatCorrect() {
		return $this->_formatCorrect;
	}
	
	public function setFormatIncorrect($_formatIncorrect) {
		$this->_formatIncorrect = (string)$_formatIncorrect;
		return $this;
	}
	
	public function getFormatIncorrect() {
		return $this->_formatIncorrect;
	}
	
	public function setGPoints($_gPoints) {
		$this->_gPoints = (int)$_gPoints;
		return $this;
	}
	
	public function getGPoints() {
		return $this->_gPoints;
    }
    
    public function setQuestionTime($_questionTime) {
		$this->_questionTime = (int)$_questionTime;
		return $this;
	}

	public function getQuestionTime() {
		return $this->_questionTime;
	}
	
	
}

add_action( 'admin_footer', 'learndash_advance_filter_js' );
function learndash_advance_filter_js(){
?>
    <script type="text/javascript">
        jQuery( document ).ready( function( $ ){
            $( '#wpProQuiz_tabHistory .postbox .inside ul li:nth-child(4)' ).append( '<input type="button" value="Filter by time" class="button-secondary" id="filter_by_time">' )
            $( '#wpProQuiz_tabHistory #filter_by_time' ).on( 'click', function(){
                $('#wpProQuiz_loadDataHistory').show();
                $( '#wpProQuiz_historyLoadContext' ).hide();
                var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
                var nonce = "<?php echo wp_create_nonce(); ?>";
                $.ajax({
                    type : "post",
                    dataType : "json",
                    url : ajaxurl,
                    data : {action: "wp_pro_quiz_admin_ajax_updated", post_id : 1, nonce: nonce},
                    success: function(response) {
                        if(response.type == "success") {
                            console.log( response.r );
                            $( '#wpProQuiz_historyLoadContext' ).html( response.r );
                        }
                        else {
                            alert("Your like could not be added");
                        }
                    },
                    complete: function(){
                        $('#wpProQuiz_loadDataHistory').hide();
                        $( '#wpProQuiz_historyLoadContext' ).show();
                    }
                });                
            } );
            
        } );
    </script>
<?php
}

add_action( 'wp_ajax_wp_pro_quiz_admin_ajax_updated', 'newstatisticLoadHistoryUpdated' );
function newstatisticLoadHistoryUpdated(){
    $nonce 	= isset($_POST['nonce']) ? $_POST['nonce'] : '';
    //$data 	= isset($_POST['data']) ? $_POST['data'] : null;

    global $wpdb;
    $where     = '';
    $timeWhere = '';
    $tableStatisticRef = $wpdb->prefix.'learndash_pro_quiz_statistic_ref';
    $tableQuestion = $wpdb->prefix.'learndash_pro_quiz_question';
    $tableStatistic = $wpdb->prefix.'learndash_pro_quiz_statistic';


    $default_args = array(
        'quizId'    => 1,
        'quiz'      => 0,
        'page'      => 0,
        'limit'     => 50,
        'users'     => -1,
        'startTime' => 0,
        'endTime'   => 0,
    );
    $args = wp_parse_args( $args, $default_args );

    switch ( $args['users'] ) {
        case -3: //only anonym
            $where = 'AND user_id = 0';
            break;
        case -2: //only reg user
            $where = 'AND user_id > 0';
            break;
        case -1: //all
            $where = '';
            break;
        default:
            $where = 'AND user_id = '.(int)$args['users'];
            break;
    }
    
    if($args['startTime'])
        $timeWhere = 'AND create_time >= '.(int)$args['startTime'];
    
    if($args['endTime'])
        $timeWhere .= ' AND create_time <= '.(int)$args['endTime'];
    
    $where = $where . ' ' . $timeWhere;

    $result = $wpdb->get_results(
        $wpdb->prepare(
            'SELECT
                u.`user_login`, u.`display_name`, u.ID AS user_id,
                sf.*,
                SUM(s.correct_count) AS correct_count,
                SUM(s.incorrect_count) AS incorrect_count,
                SUM(s.points) AS points, 
                SUM(q.points) AS g_points,
                (( 100 * s.points ) / q.points ) AS f_result,
                SUM(s.question_time) AS question_time 
            FROM
                '.$tableStatisticRef.' AS sf
                INNER JOIN '.$tableStatistic.' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
                LEFT JOIN '.$wpdb->users.' AS u ON(u.ID = sf.user_id)
                INNER JOIN '.$tableQuestion.' AS q ON(q.id = s.question_id)
            WHERE
                sf.quiz_id = %d AND sf.is_old = 0 ' . $where . '
            GROUP BY
                sf.statistic_ref_id
            ORDER BY
                f_result DESC, correct_count DESC, question_time ASC
            LIMIT
                %d, %d
        ', $args['quizId'], $args['page'], $args['limit'] ),
        ARRAY_A
    );
    
    $r = array();
    
    foreach($result as $row) {
        if(!empty($row['user_login']))
            $row['user_name'] = $row['user_login'] . ' ('. $row['display_name'] .')';
        
        $r[] = new WpProQuiz_Model_StatisticHistory_New($row);
    }
    $statisticModel = $r;
    foreach($statisticModel as $model) {
        /*@var $model WpProQuiz_Model_StatisticHistory */
        $newStat = $model->getGPoints();
        if(!$model->getUserId())
            $model->setUserName(__('Anonymous', 'learndash'));
        else if($model->getUserName() == '')
            $model->setUserName(__('Deleted user', 'learndash'));
        
        $sum = $model->getCorrectCount() + $model->getIncorrectCount();
        $result = round(100 * $model->getPoints() / $model->getGPoints(), 2).'%';
        $model->setResult($result);

        $date_format = LearnDash_Settings_Section::get_section_setting( 'LearnDash_Settings_Quizzes_Management_Display', 'statistics_time_format' );
        $date_format = trim( $date_format );
        if ( empty( $date_format ) ) {
            $date_format = 'Y/m/d g:i A';
        }
        $model->setFormatTime(
            WpProQuiz_Helper_Until::convertTime(
                $model->getCreateTime(),
                $date_format
            )
        );
        
        $model->setFormatCorrect($model->getCorrectCount().' ('.round(100 * $model->getCorrectCount() / $sum, 2).'%)');
        $model->setFormatIncorrect($model->getIncorrectCount().' ('.round(100 * $model->getIncorrectCount() / $sum, 2).'%)');
    }

    $newStat = show_history_table( $statisticModel );

    echo json_encode( array( 'type' => 'success', 'nonce' => $nonce, 'r' => $newStat ) );
    die();
}


function show_history_table( $statisticModel ){
    ob_start();
?>
    <table class="wp-list-table widefat">
        <thead>
            <tr>
                <th scope="col"><?php esc_html_e('Username', 'learndash'); ?></th>
                <th scope="col" style="width: 200px;"><?php esc_html_e('Date', 'learndash'); ?></th>
                <th scope="col" style="width: 100px;"><?php esc_html_e('Correct', 'learndash'); ?></th>
                <th scope="col" style="width: 100px;"><?php esc_html_e('Incorrect', 'learndash'); ?></th>
                <th scope="col" style="width: 100px;"><?php esc_html_e('Points', 'learndash'); ?></th>
                <th scope="col" style="width: 60px;"><?php esc_html_e('Results', 'learndash'); ?></th>
                <th scope="col" style="width: 60px;"><?php esc_html_e('Time', 'learndash'); ?><span style="font-size: x-small;">(hh:mm:ss)</span></th>
            </tr>
        </thead>
        <tbody id="wpProQuiz_statistics_form_data">
            <?php if(!count($statisticModel)) { ?>
            <tr>
                <td colspan="6" style="text-align: center; font-weight: bold; padding: 10px;"><?php esc_html_e('No data available', 'learndash'); ?></td>
            </tr>
            <?php } else { ?>
            <?php foreach($statisticModel as $model) { /* @var $model WpProQuiz_Model_StatisticHistory */ ?>
            <tr>
                <th>
                    <a href="#" class="user_statistic" data-ref_id="<?php echo $model->getStatisticRefId(); ?>"><?php echo $model->getUserName(); ?></a>
                    
                    <div class="row-actions">
                        <span>
                            <a style="color: red;" class="wpProQuiz_delete" href="#"><?php esc_html_e('Delete', 'learndash'); ?></a>
                        </span>
                    </div>
                    
                </th>
                <th><?php echo $model->getFormatTime(); ?></th>
                <th style="color: green;"><?php echo $model->getFormatCorrect(); ?></th>
                <th style="color: red;"><?php echo $model->getFormatIncorrect(); ?></th>
                <th><?php echo $model->getPoints(); ?></th>
                <th style="font-weight: bold;"><?php echo $model->getResult(); ?>%</th>
                <th style="font-weight: bold;"><?php echo WpProQuiz_Helper_Until::convertToTimeString( $model->getQuestionTime() ); ?><?php //echo $model->getQuestionTime(); ?></th>
            </tr>
            <?php } } ?>
        </tbody>
    </table>
<?php
    $html = ob_get_clean();
    return $html;
}
